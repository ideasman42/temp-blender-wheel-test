# Generate from:
# ls my_addon/wheels/ -1 | sort | cut -f1 -d-

from . import wheels

modules = []

wheel_args = [
    ("appdirs", (), None),
    ("click", (), None),
    ("elementpath", (), None),
    ("greenlet", (), None),
    ("lxml", (), None),
    ("PIL", (), "pillow"),
    ("dateutil", (), "python_dateutil"),
    (("yaml"), (), "PyYAML"),
    ("scrat", (), None),
    ("shortuuid", (), None),
    ("simple_dotdict", (), None),
    ("six", (), None),
    ("sqlalchemy", (), "SQLAlchemy"),
    ("tablib", (), None),
    ("tomlkit", (), None),
    ("typing_extensions", (), None),
    ("xmlschema", (), None),
]


ok = 0
no = 0
for args in wheel_args:
    print("Loading:", args, "...", end="")

    exception_value = None
    try:
        result = wheels.load_wheel(*args)
    except Exception as ex:
        # import traceback
        # traceback.print_exc()
        exception_value = ex
        result = None

    if result is not None:
        print("OK")
        ok += 1
    else:
        print("fail", exception_value)
        no += 1

    modules.append((args[0], result))

print("ok", ok, "fail", no)
